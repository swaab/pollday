'use strict';

var eventNames = ['start', 'end'];

/**
 * Poll
 *
 * @param {User} author
 * @param {string} title
 * @param {Array} choices
 * @return void
 */
var Poll = function(author, title, choices) {
  this.author = author;
  this.title = title;
  this.choices = choices;
  this.results = [];
  this.voters = {};
  this.listeners = {};
};

/**
 * on
 *
 * @param eventName
 * @param listener
 * @return
 */
Poll.prototype.on = function(eventName, listener) {

  // if the eventName is invalid
  if(eventNames.indexOf(eventName) === -1) {
    return false;
  }

  // init the listener event array
  if(!this.listeners[eventName]) {
    this.listeners[eventName] = [];
  }

  // register the listener
  this.listeners[eventName].push(listener);

  return this;
};

/**
 * trigger
 *
 * @param eventName
 * @return
 */
Poll.prototype.trigger = function(eventName) {
  var _this = this;

  // if the eventName is invalid
  if(!this.listeners[eventName]) {
    return;
  }

  Object.keys(this.listeners[eventName]).forEach(function(key) {
    _this.listeners[eventName][key].call();
  });

  return this;
};

/**
 * start
 *
 * @param duration
 * @return
 */
Poll.prototype.start = function(duration) {
  var _this = this;

  this.startTime = Date.now();
  this.endTime = Date.now() + duration;

  if(this.timeout) {
    clearTimeout(this.timeout);
  }

  this.timeout = setTimeout(function() {
    _this.trigger('end');
  }, duration);

  return this;
};

/**
 * stop
 *
 * @return
 */
Poll.prototype.stop = function() {
  this.endTime = Date.now();

  if(this.timeout) {
    clearTimeout(this.timeout);
  }

  this.trigger('end');

  return this;
};

/**
 * getRemainingTime
 *
 * @return integer remaining time
 */
Poll.prototype.getRemainingTime = function() {
  return this.endTime - Date.now();
};

/**
 * addExtraTime
 *
 * @param extraDuration
 * @return
 */
Poll.prototype.addExtraTime = function(extraDuration) {
  var remainingTime = this.getRemainingTime();
  this.start(remainingTime + extraDuration);
  return this;
};


/**
 * isStarted
 *
 * @return boolean
 */
Poll.prototype.isStarted = function() {
  return this.startTime && Date.now() >= this.startTime;
};

/**
 * isFinished
 *
 * @return boolean
 */
Poll.prototype.isFinished = function() {
  return this.endTime && Date.now() > this.endTime;
};

/**
 * isActive
 *
 * @return boolean
 */
Poll.prototype.isActive = function() {
  return this.isStarted() && !this.isFinished();
};

/**
 * answer
 *
 * @method answer
 * @param {User} user
 * @param choiceIndex
 * @return
 */
Poll.prototype.answer = function(user, choiceIndex) {

  // if choices index is invalid
  if(typeof this.choices[choiceIndex] === 'undefined') {
    return false;
  }

  // if user already voted
  if(this.userHasAlreadyVoted(user)) {
    return false;
  }

  // if user is author
  if(this.author.id === user.id) {
    return false;
  }

  // check the poll is active
  if(!this.isActive()) {
    return false;
  }

  if (typeof this.results[choiceIndex] !== "undefined") {
    this.results[choiceIndex]++;
  } else {
    this.results[choiceIndex] = 1;
  }

  // add user to the voters collection
  this.voters[user.id] = user;

  return true;
};

/**
 * getAnswererCount
 *
 * @return
 */
Poll.prototype.getAnswererCount = function() {
  return Object.keys(this.voters).length;
};

/**
 * userHasAlreadyVoted
 *
 * @method userHasAlreadyVoted
 * @param {User} user
 * @return
 */
Poll.prototype.userHasAlreadyVoted = function(user) {
  var userIds = Object.keys(this.voters);
  return userIds.indexOf(user.id) !== -1;
};

/**
 * getResults
 *
 * @method getResults
 * @return {Array}
 */
Poll.prototype.getResults = function() {
  var rank = [];
  for (var i in this.choices) {
     rank.push(this.results[i] || 0);
  }
  return rank;
};

Poll.prototype.getDatas = function() {
  return {
    'authorId' : this.author.id,
    'title' : this.title,
    'choices' : this.choices,
    'results' : this.getResults(),
    'answererCount' : this.getAnswererCount(),
    'remainingTime' : this.getRemainingTime()
  };
};

module.exports = Poll;

