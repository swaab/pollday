'use strict';

var Server;
var Poll = require('./Poll.js');
var User = require('./User.js');

/**
 * Server
 *
 * @param io
 * @param options
 * @return
 */
var Server = function (io, options) {
  this.options = options;
  this.io = io;

  this.currentPoll = undefined;
  this.connectedUsers = 0;
  this.answererCount = 0;
  this.logger = console;

  this.userEventNames = ['initPoll', 'newVote', 'endPoll', 'newPoll', 'disconnect', 'reset', 'extraTime'];


  // Init status
  this.status = this.STATUS_INIT_POLL;

};

Server.prototype.start = function () {
  var _this = this;
  this.io.sockets.on('connection', function(socket) {
    var user = new User(socket);

    _this.onConnection(user);

    // register user events callbacks
    _this.userEventNames.forEach(function(eventName) {
      user.socket.on(eventName, function() {
        var args = Array.prototype.slice.call(arguments);
        args.unshift(user);
        _this['on' + eventName.charAt(0).toUpperCase() + eventName.slice(1)].apply(_this, args);
      });
    });
  });
};

Server.prototype.STATUS_INIT_POLL        = 0;
Server.prototype.STATUS_CREATE_POLL      = 1;
Server.prototype.STATUS_POLL_IN_PROGRESS = 2;
Server.prototype.STATUS_POLL_ENDED       = 3;

/**
 * onConnection
 *
 * @param user
 * @return
 */
Server.prototype.onConnection = function (user) {
  this.connectedUsers++;

  this.io.sockets.emit('connectedUsers', this.connectedUsers);

  user.socket.emit('status', this.status);

  if(this.currentPoll) {
    user.socket.emit('newPoll', this.currentPoll.getDatas());
  }
};

/**
 * onReset
 *
 * @param user
 * @return
 */
Server.prototype.onReset = function (user, value, fn) {
  if(!this.currentPoll) {
    if(fn) { fn.call(this, false); }
    return false;
  }

  // if the user is not the author do nothing
  if(user.id !== this.currentPoll.author.id) {
    if(fn) { fn.call(this, false); }
    return false;
  }

  this.reset();
};

/**
 * reset
 *
 * @return
 */
Server.prototype.reset = function() {
  // if the user is the current poll author update status
  this.updateStatus(this.STATUS_INIT_POLL);

  // if the user is the current poll author current poll must be empty
  this.currentPoll = undefined;
};

/**
 * onNewVote
 *
 * @param user
 * @param answerIndex
 * @return
 */
Server.prototype.onNewVote = function (user, answerIndex, fn) {

  if(!this.currentPoll) {
    return false;
  }

  if(!this.currentPoll.answer(user, answerIndex)) {
    if (fn) { fn.call(this, false); }
    return false;
  }

  // broadcast new answererCount
  this.io.sockets.emit('answererCount', this.currentPoll.getAnswererCount());

  if (fn) { fn.call(this, true); }
};

/**
 * onEndPoll
 *
 * @return
 */
Server.prototype.onEndPoll = function(user) {
  if(!this.currentPoll || this.currentPoll.author.id !== user.id) {
    return false;
  }
  this.currentPoll.stop();
};

/**
 * onInitPoll
 *
 * @return
 */
Server.prototype.onInitPoll = function(user, value, fn) {
  // clear old timeout
  clearTimeout(this.endTimeout);

  if (this.status === this.STATUS_INIT_POLL) {
    this.currentPoll = new Poll(user, '', []);
    this.updateStatus(this.STATUS_CREATE_POLL);
    user.socket.broadcast.emit('newPoll', this.currentPoll.getDatas());
    if (fn) { fn.call(this, true); }
  } else {
    if (fn) { fn.call(this, false); }
  }
};

/**
 * onNewPoll
 *
 * @param user
 * @param pollDatas
 * @return
 */
Server.prototype.onNewPoll = function(user, pollDatas) {
  var _this = this;

  this.updateStatus(this.STATUS_POLL_IN_PROGRESS);
  if (this.currentPoll) {
    this.currentPoll.title = pollDatas.title;
    this.currentPoll.choices = pollDatas.choices;
  }
  else {
    this.currentPoll = new Poll(user, pollDatas.title, pollDatas.choices);
  }

  this.currentPoll.start(pollDatas.duration);

  this.currentPoll.on('end', function() {
    _this.endCurrentPoll();
  });

  user.socket.broadcast.emit('newPoll', this.currentPoll.getDatas());
};

/**
 * onDisconnect
 *
 * @param user
 * @return
 */
Server.prototype.onDisconnect = function(user) {
  this.connectedUsers--;

  // if user is author of the current poll end it.
  if(this.currentPoll && this.currentPoll.author.id === user.id) {
    if(this.status === this.STATUS_CREATE_POLL) {
      this.updateStatus(this.STATUS_INIT_POLL);
    }
    else {
      this.currentPoll.stop();
    }
  }

  this.io.sockets.emit('connectedUsers', this.connectedUsers);
};

/**
 * onExtraTime
 *
 * @return
 */
Server.prototype.onExtraTime = function(user, extraTime) {
  if(this.currentPoll && this.currentPoll.author.id === user.id) {
    this.currentPoll.addExtraTime(extraTime);
    this.io.sockets.emit('extraTime', extraTime);
  }
};

/**
 * updateStatus
 *
 * @param status
 * @return
 */
Server.prototype.updateStatus = function(status) {
  this.status = status;
  this.io.sockets.emit('status', this.status);
};

/**
 * endCurrentPoll
 *
 * @return
 */
Server.prototype.endCurrentPoll = function() {
  if(!this.currentPoll) {
    return false;
  }

  var _this = this;
  this.updateStatus(this.STATUS_POLL_ENDED);
  this.io.sockets.emit('results', this.currentPoll.getResults());
  this.endTimeout = setTimeout(function() {
    _this.reset();
  }, this.options.timeout * 1000);
};

/**
 * shutdown
 *
 * @return
 */
Server.prototype.shutdown = function() {
  return this.io.sockets.clients().forEach(function(socket) {
    return socket.disconnect();
  });
};

module.exports = Server;

