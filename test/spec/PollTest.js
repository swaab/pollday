/* global describe, it, beforeEach, before, after */
/* jshint expr:true */

(function () {
  'use strict';

  var chai = require('chai');
  var expect = chai.expect;
  var sinon = require('sinon');
  var Poll = require('../../lib/Poll.js');

  describe('Poll', function () {

    var author = {
      'id': 'id1'
    };

    var title = "Foo";

    var choices = [
      'choice 1',
      'choice 2',
      'choice 3'
    ];

    var poll;

    beforeEach(function(){
      poll = new Poll(author, title, choices);
    });

    describe('#on', function () {

      it('should register the listener with the correct eventName', function () {
        var listener = sinon.stub();
        poll.on('start', listener);
        expect(poll.listeners['start'][0]).to.equal(listener);
      });

    });

    describe('#trigger', function () {

      it('should call event listeners', function () {
        var listener = sinon.stub();

        poll.listeners.start = [listener, listener];

        poll.trigger('start');

        expect(listener.calledTwice).to.be.true;
      });

    });

    describe('#start', function () {

      var clock;

      before(function () { clock = sinon.useFakeTimers(); });
      after(function () { clock.restore(); });

      it('should set startTime and end time to the poll', function () {
        var triggerStub = sinon.stub(poll, 'trigger');
        var previousTimeoutCallback = sinon.stub();

        poll.timeout = setTimeout(previousTimeoutCallback, 20);

        poll.start(30);
        expect(poll.startTime).to.equal(0);
        expect(poll.endTime).to.equal(30);

        // should trigger event end at the endTime
        clock.tick(29);
        expect(triggerStub.called).to.be.false;
        clock.tick(1);
        expect(triggerStub.calledWith('end')).to.be.true;

        // check that previous timeout has been reseted
        expect(previousTimeoutCallback.called).to.be.false;
      });
    });

    describe('#stop', function () {

      var clock;

      before(function () { clock = sinon.useFakeTimers(); });
      after(function () { clock.restore(); });

      it('should set endTime to now', function () {
        clock.tick(20);

        poll.stop();

        expect(poll.endTime).to.equal(20);
      });

    });

    describe('#isFinished', function () {

      var clock;

      before(function () { clock = sinon.useFakeTimers(); });
      after(function () { clock.restore(); });

      it('should return true if poll is finished', function () {

        poll.startTime = Date.now();
        poll.endTime = Date.now() + 20;

        clock.tick(20);

        expect(poll.isFinished()).to.be.false;

        clock.tick(1);

        expect(poll.isFinished()).to.be.true;
      });

    });

    describe('#isStarted', function () {

      var clock;

      before(function () { clock = sinon.useFakeTimers(); });
      after(function () { clock.restore(); });

      it('should return true if poll is started', function () {

        poll.startTime = Date.now() + 10;

        expect(poll.isStarted()).to.be.false;

        clock.tick(10);

        expect(poll.isStarted()).to.be.true;
      });

    });

    describe('#isActive', function () {

      it('should return true if poll is started and not finished', function () {
        sinon.stub(poll, 'isStarted').returns(true);
        sinon.stub(poll, 'isFinished').returns(false);

        expect(poll.isActive()).to.be.true;

        poll.isStarted.returns(false);

        expect(poll.isActive()).to.be.false;

        poll.isStarted.returns(false);
        poll.isFinished.returns(true);

        expect(poll.isActive()).to.be.false;

        poll.isStarted.restore();
        poll.isFinished.restore();
      });

    });

    describe('#stop', function () {

      var clock;

      before(function () { clock = sinon.useFakeTimers(); });
      after(function () { clock.restore(); });

      it('should set endTime to now', function () {
        clock.tick(20);

        poll.stop();

        expect(poll.endTime).to.equal(20);
      });

    });

    describe('#answer', function () {
      var answerer1 = { 'id' : 'id1' };
      var answerer2 = { 'id' : 'id2' };

      beforeEach(function() {
        sinon.stub(poll, 'isActive').returns(true);
      });

      it('should save user answer', function () {
        var returnedValue = poll.answer(answerer2, 1);
        expect(poll.results[1]).to.equal(1);
        expect(returnedValue).to.be.true;
      });
      it('should accept only one vote by user', function () {
        // first answer
        poll.answer(answerer2, 1);
        // second answer
        var returnedValue = poll.answer(answerer2, 1);
        expect(returnedValue).to.be.false;
      });
      it('should reject author vote', function () {
        var returnedValue = poll.answer(answerer1, 1);
        expect(returnedValue).to.be.false;
      });
      it('should reject user answer if answerIndex is invalid', function () {
        var returnedValue = poll.answer(answerer2, 5);
        expect(returnedValue).to.be.false;
      });
      it('should reject user vote is the poll is not active', function () {
        poll.isActive.returns(false);
        var returnedValue = poll.answer(answerer2, 1);
        expect(returnedValue).to.be.false;
      });
    });

    describe('#getAnswerCount', function () {
      it('should return the correct answerer count', function () {
        sinon.stub(poll, 'isActive').returns(true);

        var answerer1 = { 'id' : 'id2'};
        var answerer2 = { 'id' : 'id3'};

        poll.answer(answerer1, 1);
        poll.answer(answerer1, 2);
        poll.answer(answerer2, 0);

        expect(poll.getAnswererCount()).to.equal(2);

        poll.isActive.restore();
      });
    });

  });
})();
